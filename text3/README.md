# 实验3：创建分区表

学号：202010112216 姓名：李锴

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。

- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。

- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。

- orders表按订单日期（order_date）设置范围分区。

- order_details表设置引用分区。

- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。

- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
  -进行分区与不分区的对比实验。

  

### 创建sale用户

```sql
  SQL> show user
  USER 为 "SALE"
```

### 创建orders订单表
```sql
CREATE TABLE orders 
(
order_id NUMBER(9, 0) NOT NULL,
customer_name VARCHAR2(40 BYTE) NOT NULL,
customer_tel VARCHAR2(40 BYTE) NOT NULL,
order_date DATE NOT NULL,
employee_id NUMBER(6, 0) NOT NULL, 
discount NUMBER(8, 2) DEFAULT 0, 
trade_receivable NUMBER(8, 2) DEFAULT 0,
CONSTRAINT ORDERS_PK PRIMARY KEY 
  (
    ORDER_ID 
  )
) 
TABLESPACE SALETAB 
PCTFREE 10 INITRANS 1 
STORAGE (BUFFER_POOL DEFAULT) 
NOCOMPRESS NOPARALLEL 
PARTITION BY RANGE (order_date) 
( PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
 TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
 'NLS_CALENDAR=GREGORIAN')) 
 NOLOGGING
 TABLESPACE SALETAB
 PCTFREE 10 
 INITRANS 1 
 STORAGE( 
 INITIAL 8388608 
 NEXT 1048576 
 MINEXTENTS 1 
 MAXEXTENTS UNLIMITED 
 BUFFER_POOL DEFAULT) 
NOCOMPRESS NO INMEMORY  
, PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING
TABLESPACE SALETAB,
PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
'NLS_CALENDAR=GREGORIAN')) 
NOLOGGING 
TABLESPACE SALETAB
);
表已创建。
```
### 创建order_details表的语句如下：

```sql
CREATE TABLE order_details
( id NUMBER(9, 0) NOT NULL,
order_id NUMBER(10, 0) NOT NULL,
product_id VARCHAR2(40 BYTE) NOT NULL, 
product_num NUMBER(8, 2) NOT NULL, 
product_price NUMBER(8, 2) NOT NULL, 
CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
  (
    id 
  ),
CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
REFERENCES orders  (  order_id   )
ENABLE
) 
TABLESPACE SALETAB
PCTFREE 10 INITRANS 1 
STORAGE ( BUFFER_POOL DEFAULT ) 
NOCOMPRESS NOPARALLEL
PARTITION BY REFERENCE (order_details_fk1);
表已创建。
```
### 创建序列SEQ1的语句如下
```sql
CREATE SEQUENCE  SEQ1  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
序列已创建。
```
### 插入100条orders记录的样例
```sql
declare 
   i integer;
   y integer;
   m integer;
   d integer;
   str varchar2(100);
BEGIN  
  i:=0;
  y:=2015;
  m:=1;
  d:=12;
  while i<100 loop
    i:= i+1;
    m:=m+1;
    if m>12 then
        m:=1;
    end if;
    str:=y||'-'||m||'-'||d;
    insert into orders(order_id,order_date) values(SEQ1.nextval,to_date(str,'yyyy-MM-dd'));
  end loop;
  commit;
END;
/

```

#### 比较
- 分区与不分区在数据量非常大的时候对比非常明显，当数据量庞大的时候，分区的优势就会特别明显，查询的速度就会非常的快
- 在数据量比较小的时候，二者的区别就不会很大，查询的速度不会有特别大的明显，都比较快。

## 实验图片

![img1](img1.jpg)

![img1](img2.png)

![img1](img3.png)

![img1](img4.png)

## 实验总结

通过本次实验，我们学习了如何创建分区表和非分区表，并插入大量数据。我们还比较了这两种表的查询执行计划，发现分区表的查询速度更快。这是因为分区表只需要查询相关分区，而非分区表则需要扫描整个表。

在实际开发中，我们可以根据数据量和查询需求来选择使用分区表。如果表中数据量大且需要经常查询，那么使用分区表可以提高查询效率，降低数据管理复杂度。

在创建分区表时，需要注意一些细节问题。例如，我们需要选择合适的分区键以避免数据倾斜，并根据实际情况设置分区策略以确保数据均匀分布到各个分区中。

此外，我们还学习了如何创建索引和序列。这些知识在数据库管理和性能优化中非常有用。

总之，本次实验为我们提供了深入了解分区表及其用法的机会，并且使我们更加熟练地掌握了Oracle数据库的创建表、创建索引、创建序列和分区等相关操作。
