--------------------------------------------------------
--  文件已创建 - 星期四-五月-25-2023   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body SALES_PACKAGE
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE PACKAGE BODY "SYSTEM"."SALES_PACKAGE" as
 procedure add_order(p_customer_id in number, p_product_list in varchar2)
is
l_order_id number := 0;
l_product_id number := 0;
l_quantity number := 0;
l_product_price number := 0;
l_order_total number := 0;
n number := 0 ;
begin
  select max(order_id) into l_order_id from orders;
  insert into orders(order_id,order_date,customer_id) values (l_order_id+1,sysdate,(p_customer_id));
  for i in 1 .. length(p_product_list) loop
   if substr(p_product_list, i, 1) = ',' then
       select product_price into l_product_price from products where product_id = l_product_id;
       l_order_total := l_order_total + l_product_price * l_quantity;
       insert into order_details (order_id, product_id, quantity) values (l_order_id, l_product_id, l_quantity);
       l_product_id := 0;
       l_quantity := 0;
    else
       if substr(p_product_list,i,1)<> '|' then
         l_product_id := l_product_id*10 + to_number(substr(p_product_list,i,1));
       else
         l_quantity := to_number(substr(p_product_list, i+1, 1));
         n :=  n+1;
     end if;
   end if;
end loop;

select product_price into l_product_price from products where product_id = l_product_id;
l_order_total := l_order_total + l_product_price*l_quantity;
insert into order_details (order_id, product_id, quantity) values (l_order_id, l_product_id, l_quantity);
update orders set total = l_order_total where order_id = l_order_id;
end add_order;

function get_order_total(p_order_id in number) return number is
    l_total number;
begin
  select total into l_total from orders where order_id = p_order_id;
  return l_total;
end get_order_total;

procedure update_order(p_order_id in number, p_product_id in number, p_quantity in number)
is
l_product_price number := 0;
l_order_total number := 0;
begin
  select product_price into l_product_price from products where product_id = p_product_id;
  update order_details set quantity = p_quantity where order_id = p_order_id and product_id = p_product_id;
select sum(product_price * quantity) into l_order_total from order_details od join products p on od.product_id = p.product_id where order_id = p_order_id;
update orders set total = l_order_total where order_id = p_order_id;
end update_order;

end sales_package;

/
