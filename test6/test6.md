<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|



### 创建表空间
```sql
create tablespace sales_data datafile '/tmp/sales_data.dbf' size 50m autoextend on next 20m maxsize unlimited;
表空间已创建。

create tablespace sales_index datafile '/tmp/sales_index.dbf' size 50m autoextend on next 20m maxsize unlimited;
表空间已创建。
```

### 创建表
#### 创建产品表
```sql
SQL> create table products(
  product_id number(10) primary key,
  product_name varchar2(50),
  product_price number(10,2),
  product_quantity number(10)
  ) tablespace sales_data;
表已创建。
```

#### 创建用户表
```sql
SQL> create table customers (
  customer_id number(10) primary key,
  customer_name varchar2(50),
  customer_phone varchar2(20)
  ) tablespace sales_data;
表已创建。
```
#### 创建订单表
```sql
SQL> create table orders (
  order_id number(10) primary key,
  order_date date,
  customer_id number(10),
  foreign key (customer_id) references customers(customer_id)
  ) tablespace sales_data;
表已创建。
```
#### 创建订单详细表
```sql
SQL> create table order_details (
  order_id number(10),
  product_id number(10),
  quantity number(10),
  foreign key (order_id) references orders(order_id),
  foreign key (product_id) references products(product_id)
  ) tablespace sales_data;
表已创建。
```

### 插入数据
#### 插入10条产品信息
```sql
SQL> declare
  begin
  for i in 1..10 loop
  insert into products values (i, 'product'||i, floor(dbms_random.value(10,10000))/100, floor(dbms_random.value(10,100)));  
  end loop;
  commit;
  end;
  /
PL/SQL 过程已成功完成。
```
### 插入100条用户信息
```sql
SQL> begin
  for i in 1..100 loop
  insert into customers values (i, 'customer'||i, '138'||rpad(to_char(i),8,'0'));
  end loop; 
  commit;
  end;
  /
PL/SQL 过程已成功完成。
```
### 创建1000个订单信息
```sql
declare 
  l_customer_id number(10);
  begin
  for i in 1..1000 loop
  l_customer_id := floor(dbms_random.value(1,100));
  insert into orders values (i, sysdate, l_customer_id);
  for j in 1..floor(dbms_random.value(1,5)) loop
  insert into order_details values (i, floor(dbms_random.value(1,10)), 
  floor(dbms_random.value(1,10)));
  end loop;
  end loop;
  commit;
  end;
  /
PL/SQL 过程已成功完成。
```
### 插入更多数据
```sql
SQL> declare 
  begin
  for i in 11..110 loop
  insert into products values (i, 'product'||i, floor(dbms_random.value(10,10000))/100, floor(dbms_random.value(10,100))); 
  end loop;
  commit;
  end;
  /
PL/SQL 过程已成功完成。

SQL> declare
  begin
  for i in 101..1100 loop
  insert into customers values (i, 'customer'||i, '138'||rpad(to_char(i),8,'0'));
  end loop;
  commit;
  end;
  /
PL/SQL 过程已成功完成。

SQL> declare 
  l_customer_id number(10);
  begin
  for i in 1001..11000 loop
  l_customer_id := floor(dbms_random.value(1,1000));
  insert into orders values (i, sysdate, l_customer_id);
  for j in 1..floor(dbms_random.value(1,5)) loop
  insert into order_details values (i, floor(dbms_random.value(1,100)), floor(dbms_random.value(1,10)));
  end loop;
  end loop;
  commit;
  end;
  /
PL/SQL 过程已成功完成。
```
### 创建包
```sql
SQL> create or replace package sales_package as
  procedure add_order(p_customer_id in number, p_product_list in varchar2);
  function get_order_total(p_order_id in number) return number;
  procedure update_order(p_order_id in number, p_product_id in number, p_quantity in number);
  end sales_package;
  /
程序包已创建。

```

### 创建包体
```sql
create or replace NONEDITIONABLE package body sales_package as
 procedure add_order(p_customer_id in number, p_product_list in varchar2)
is
l_order_id number := 0;
l_product_id number := 0;
l_quantity number := 0;
l_product_price number := 0;
l_order_total number := 0;
n number := 0 ;
begin
  select max(order_id) into l_order_id from orders;
  insert into orders(order_id,order_date,customer_id) values (l_order_id+1,sysdate,(p_customer_id));
  for i in 1 .. length(p_product_list) loop
   if substr(p_product_list, i, 1) = ',' then
       select product_price into l_product_price from products where product_id = l_product_id;
       l_order_total := l_order_total + l_product_price * l_quantity;
       insert into order_details (order_id, product_id, quantity) values (l_order_id, l_product_id, l_quantity);
       l_product_id := 0;
       l_quantity := 0;
    else
       if substr(p_product_list,i,1)<> '|' then
         l_product_id := l_product_id*10 + to_number(substr(p_product_list,i,1));
       else
         l_quantity := to_number(substr(p_product_list, i+1, 1));
         n :=  n+1;
     end if;
   end if;
end loop;

select product_price into l_product_price from products where product_id = l_product_id;
l_order_total := l_order_total + l_product_price*l_quantity;
insert into order_details (order_id, product_id, quantity) values (l_order_id, l_product_id, l_quantity);
update orders set total = l_order_total where order_id = l_order_id;
end add_order;

function get_order_total(p_order_id in number) return number is
    l_total number;
begin
  select total into l_total from orders where order_id = p_order_id;
  return l_total;
end get_order_total;

procedure update_order(p_order_id in number, p_product_id in number, p_quantity in number)
is
l_product_price number := 0;
l_order_total number := 0;
begin
  select product_price into l_product_price from products where product_id = p_product_id;
  update order_details set quantity = p_quantity where order_id = p_order_id and product_id = p_product_id;
select sum(product_price * quantity) into l_order_total from order_details od join products p on od.product_id = p.product_id where order_id = p_order_id;
update orders set total = l_order_total where order_id = p_order_id;
end update_order;

end sales_package;


```

### 创建用户表
```sql
SQL> create user sales_manager identified by manager_password default tablespace sales_data quota unlimited on sales_data;
用户已创建。

SQL> create user sales_representative identified by representative_password default tablespace sales_data quota unlimited on sales_data;
用户已创建。
```

### 分配权限
```sql
SQL> grant connect, resource to sales_manager;
授权成功。

SQL> grant connect, resource to sales_representative;
授权成功。

SQL> grant all privileges to sales_manager;
授权成功。

SQL> grant select, insert, update, delete on customers to sales_representative;
授权成功。

SQL> grant select, insert, update, delete on orders to sales_representative; 
授权成功。

SQL> grant select, insert, update, delete on order_details to sales_representative; 
授权成功。

```

### 数据库备份方案
- 可以考虑使用RMAN（Recovery Manager）备份和恢复工具来备份Oracle数据库。RMAN是Oracle官方提供的备份工具，具有以下优点：
- 1、支持全备、增量备、增量合并备份等多种备份方式；
- 2、备份速度快，可压缩备份数据；
- 3、支持增量恢复，可快速恢复数据库；
- 4、支持自动备份和恢复；
- 5、可以备份到磁盘或磁带，并且支持多种备份格式。
- 使用RMAN备份数据库的步骤如下：
- 1、创建RMAN备份脚本；
- 2、运行RMAN备份脚本备份数据库；
- 3、定期测试备份文件的可用性；
- 4、如有需要，可以将备份文件复制到其他地方进行存储。
- 需要注意的是，在备份数据库之前，需要先进行数据库的归档操作，以确保备份文件包含了所有的数据库更改。另外，备份文件的存储位置也需要进行合理的规划，以保证- 备份文件的安全性和可用性。


