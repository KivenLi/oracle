--------------------------------------------------------
--  文件已创建 - 星期四-五月-25-2023   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package SALES_PACKAGE
--------------------------------------------------------

  CREATE OR REPLACE NONEDITIONABLE PACKAGE "SYSTEM"."SALES_PACKAGE" as
procedure add_order(p_customer_id in number, p_product_list in varchar2);
 function get_order_total(p_order_id in number) return number;
procedure update_order(p_order_id in number, p_product_id in number, p_quantity in number);
end sales_package;

/
