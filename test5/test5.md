# 实验5：包，过程，函数的用法

姓名:李锴      学号:202010112216

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工

# 实验步骤

一、创建包(MyPack)

1.1 包的概念

包是一个可重用的程序单元，可以用来封装PL/SQL代码和数据。它由两部分组成：包头和包体。包头中声明了包中可见的变量、常量、类型和子程序的名称、参数类型和返回值类型等信息；包体中实现了包头中声明的子程序的具体逻辑。包可以包含多个子程序和变量，其可以被其他程序或包调用，可以提高代码的复用性和可维护性。

1.2 创建包(MyPack)

在Oracle数据库中，可以通过CREATE PACKAGE语句来创建一个包。下面是创建包(MyPack)的代码：

```
sqlCopy codeCREATE OR REPLACE PACKAGE MyPack AS
  FUNCTION Get_SalaryAmount(dept_id NUMBER) RETURN NUMBER;
  PROCEDURE GET_EMPLOYEES(emp_id NUMBER);
END MyPack;
/
```

这里我们声明了两个子程序：Get_SalaryAmount和GET_EMPLOYEES，分别用于计算部门工资总额和查询某个员工及其所有下属员工的信息。注意，这里使用了AS关键字来分隔包头和包体的声明。

1.3 实现包体(MyPack)

包体中实现了包头中声明的子程序的具体逻辑。下面是实现包体(MyPack)的代码：

```sql
sqlCopy codeCREATE OR REPLACE PACKAGE BODY MyPack AS
  FUNCTION Get_SalaryAmount(dept_id NUMBER) RETURN NUMBER IS
    total_salary NUMBER := 0;
  BEGIN
    SELECT SUM(salary) INTO total_salary
    FROM employees
    WHERE department_id = dept_id;
    RETURN total_salary;
  END;

  PROCEDURE GET_EMPLOYEES(emp_id NUMBER) IS
    CURSOR subordinates_cur (emp_id NUMBER) IS
      SELECT employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, commission_pct, manager_id, department_id
      FROM employees
      WHERE manager_id = emp_id;
    subordinate_rec employees%ROWTYPE;
  BEGIN
    -- 查询当前员工的信息
    SELECT * INTO subordinate_rec
    FROM employees
    WHERE employee_id = emp_id;
    DBMS_OUTPUT.PUT_LINE(subordinate_rec.first_name || ' ' || subordinate_rec.last_name || ' (' || subordinate_rec.email || ')');

    -- 递归查询下属员工
    FOR sub_rec IN subordinates_cur(emp_id) LOOP
      FOR i IN 1..level LOOP
        DBMS_OUTPUT.PUT('--');
      END LOOP;
      DBMS_OUTPUT.PUT(sub_rec.first_name || ' ' || sub_rec.last_name || ' (' || sub_rec.email || ')');
      DBMS_OUTPUT.NEW_LINE;
      -- 递归调用子程序
      GET_EMPLOYEES(subordinate_rec.employee_id);
END LOOP;
END;
END MyPack;
/


```

>  这里我们实现了包体中的两个子程序：Get_SalaryAmount和GET_EMPLOYEES。Get_SalaryAmount函数用于计算部门工资总额，查询employees表中指定部门(dept_id)的所有员工的工资并求和，返回总额；GET_EMPLOYEES过程用于查询某个员工及其所有下属员工的信息，通过使用游标来实现递归查询。 
>
> 二、使用包(MyPack) 在创建了包(MyPack)之后，我们可以通过调用其中的子程序来实现我们的功能。下面是使用包(MyPack)的代码示例： 2.1 调用Get_SalaryAmount函数 我们可以使用以下代码来调用Get_SalaryAmount函数，查询部门(department_id=80)的工资总额： ```
>
> ```sql
> sql DECLARE  total_salary NUMBER; BEGIN  total_salary := MyPack.Get_SalaryAmount(80);  DBMS_OUTPUT.PUT_LINE('The total salary of department 80 is: ' || total_salary); END; /
> ```
>

# 实验截图

![](./test_img.png)

![](./test_img2.png)![image-20230515105058671](test_img6.png)

# 实验总结

> 在PL/SQL中，包（package）、过程（procedure）和函数（function）是常用的语言结构，用于组织和封装代码，提供模块化和可重用性。包、过程和函数是PL/SQL中常用的语言结构，它们提供了一种模块化和封装代码的方式，提高了代码的可维护性、可读性和可重用性。通过实验使用这些结构，我学会了如何将代码组织为更小的模块，提高团队合作和并行开发的效率。我还了解到如何使用过程来执行特定任务和控制事务，以及如何使用函数进行计算和处理数据。这些结构对于编写高效、可靠和易于维护的PL/SQL代码非常有帮助。